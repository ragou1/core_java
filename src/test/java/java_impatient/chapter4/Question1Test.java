package java_impatient.chapter4;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.*;

import java.util.List;

import static java_impatient.common.TestUtils.getFilePath;

public class Question1Test {
    BufferedReader fileReader;

    @BeforeClass
    public void loadFile() throws FileNotFoundException {
        fileReader = new BufferedReader(new FileReader(
                getClass().getClassLoader().getResource("chapter4/question1_valid.txt").getFile()
        )
        );
    }

    @AfterClass
    public void closeFile() throws IOException {
        if (fileReader != null) {
            fileReader.close();
        }
    }

    @Test(expectedExceptions = FileNotFoundException.class)
    public void throwsExWhenFilePathIsWrong() throws IOException {
        Question1.readValues("invalid/file/path");
    }

    @Test
    public void returnsEmptyListForEmptyFile() throws Exception {
        //Is this the best way to get the full path of a file?
        List<Double> values = Question1.readValues( getFilePath("chapter4/question1_empty.txt") );
        Assert.assertTrue(values.isEmpty(), "List is not empty as expected!");
    }

    @Test
    public void returnsFloatsInFile() throws Exception {
        List<Double> values = Question1.readValues( getFilePath("chapter4/question1_valid.txt") );
        Assert.assertEquals(values.size(), 5, "List is not empty as expected!");
    }

    @Test(expectedExceptions = NumberFormatException.class)
    public void throwsExWhenFileHasNonFloats() throws Exception {
        Question1.readValues( getFilePath("chapter4/question1_invalid.txt") );
    }

}
