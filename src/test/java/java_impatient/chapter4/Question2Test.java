package java_impatient.chapter4;

import java_impatient.common.TestUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class Question2Test {
    @Test
    public void canSumDoublesInFile() throws IOException {
        double sum = Question2.sumOfValues(TestUtils.getFilePath("chapter4/question1_valid.txt"));
        Assert.assertEquals(sum, 26.5, "Sum of numbers is wrong!");
    }
}
