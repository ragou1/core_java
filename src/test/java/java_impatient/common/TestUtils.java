package java_impatient.common;

import java.io.File;

public class TestUtils {
    private static final String RESOURCE_BASE_PATH = "src/test/resources/";

    //@relativePath is relative to test/resources directory.
    public static String getFilePath(String relativePath){
        File file = new File(RESOURCE_BASE_PATH + relativePath);
        String fullPath = file.getAbsolutePath();
        return fullPath;
    }

}
