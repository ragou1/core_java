package java_impatient.chapter1;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Question2Test {
    @Test(dataProvider = "angles", description = "using modulo operator")
    public void v1TestAngleNormalization(int angleInDegrees, int normalizedDegree){
        Assert.assertEquals(Question2.normalizeAnglesV1(angleInDegrees), normalizedDegree);
    }

    @Test(dataProvider = "angles", description = "using Math.floorMod()")
    public void v2TestAngleNormalization(int angleInDegrees, int normalizedDegree){
        Assert.assertEquals(Question2.normalizeAnglesV2(angleInDegrees), normalizedDegree);
    }

    @DataProvider(name = "angles")
    public static Object [] [] anglesData(){
        return new Object[][] {
                {0,0},
                {360, 0},
                {10, 10},
                {-10, 350},
                {720, 0},
                {730, 10},
                {-730, 350}
        };
    }
}
