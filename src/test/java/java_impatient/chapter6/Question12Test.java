package java_impatient.chapter6;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Question12Test {
    @Test
    public void findMinInIntegerList(){
        List<Integer> list = Stream.of(5,2,4,1,3).collect(Collectors.toList());
        List<Integer> minMax = new ArrayList<>(2);
        Lists.minmax(list, Comparator.comparingInt(a -> a), minMax);
        Assert.assertEquals(minMax.get(0), Integer.valueOf(1));
    }

    @Test
    public void findMaxInIntegerList(){
        List<Integer> list = Stream.of(5,2,4,1,3).collect(Collectors.toList());
        List<Integer> minMax = new ArrayList<>(2);
        Lists.minmax(list, Comparator.comparingInt(a -> a), minMax);
        Assert.assertEquals(minMax.get(1), Integer.valueOf(5));
    }

}
