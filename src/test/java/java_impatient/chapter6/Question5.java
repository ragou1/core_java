package java_impatient.chapter6;

public class Question5 {
    public static void main(String [] args){
        //See end comments for explanation of these compiler errors.

        //Double[] result = Arrays.swap(0, 1, 1.5, 2, 3);//ERROR 1 - Confusing compiler error !
        //result = Arrays.<Double>swap(0, 1, 1.5, 2, 3);//ERROR 2 - Easily understandable compiler error !
    }

    private static class Arrays{
        //Swap two elements in array.
        public static <T> T[] swap(int i, int j, T... values) {
            T temp = values[i];
            values[i] = values[j];
            values[j] = temp;
            return values;
        }
    }

    /*
    Note - The errors below are from JDK's javac compiler and not from any IDE. They can be different.

    ERROR 1 -

    Error:(5, 31) java: incompatible types: inference variable T has incompatible bounds :
    lower bounds: java.lang.Double,java.lang.Object
    lower bounds: java.lang.Integer,java.lang.Double

    Comments - Very confusing. How to fix the problem when it is not clear what is the problem ?

    ERROR 2 -

    method swap in class Test.Arrays cannot be applied to given types;
    required: int,int,T[]
    found: int,int,double,int,int
    reason: varargs mismatch; int cannot be converted to java.lang.Double.

    Comment - This error is very clear unlike the previous one. The fix is now obvious, i.e make all relevant
    numbers as double (ex. 2.0, 3.0).
    */
}
