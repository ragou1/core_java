package java_impatient.chapter6;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Question7n8Test {
    /*
    Implement a class Pair<E> that stores a pair of elements of type E. Provide accessors to get the
    first and second element.
    Modify the class of the preceding exercise by adding methods max and min, getting the larger or smaller
    of the two elements. Supply an appropriate type bound for E.
    * */

    @DataProvider(name = "objectCreation")
    public Object [] [] testObjects(){
       Object [] [] allData = new Object[][]{
               {"Integers.", 5, 10},
               {"Custom objects.", new Person("James Dean", 20), new Person("Sarah Lee", 25) }
       };

       for(Object [] data : allData){
           int lastIdx = data.length-1;
           Comparable a = (Comparable) data[lastIdx-1];
           Comparable b = (Comparable) data[lastIdx];
           Assert.assertTrue(a.compareTo(b) < 0, "Please arrange your data such that a < b");
       }

       return allData;
    }

    @Test(dataProvider = "objectCreation")
    public <T extends Comparable> void createPairs(Object type, Object small, Object big){
        System.out.println("Testing with " + type);
        Pair<T> pair = new Pair<>( (T) small, (T) big);
        Assert.assertEquals(pair.getFirst(), small);
        Assert.assertEquals(pair.getSecond(), big);
    }

    @Test(dataProvider = "objectCreation")
    public <T extends Comparable> void getMaxInPairWithUnequalItems(Object type, Object small, Object big){
        System.out.println("Testing with " + type);
        //Test left < right
        Pair<T> pair = new Pair<>( (T) small, (T) big);
        Assert.assertEquals(pair.max(), big);

        //Test left > right
        pair = new Pair<>( (T) big, (T) small);
        Assert.assertEquals(pair.max(), big);
    }

    @Test
    public void getMaxInPairWithEqualItems(){
        Pair<Integer> pair = new Pair<>(5, 5);
        Assert.assertEquals(pair.max(), Integer.valueOf(5));
    }

    @Test
    public void getMinInPairWithUnequalItems(){
        //Test left < right
        Pair<Integer> pair = new Pair<>(5, 10);
        Assert.assertEquals(pair.min(), Integer.valueOf(5));

        //Test left > right
        pair = new Pair<>(150, 25);
        Assert.assertEquals(pair.min(), Integer.valueOf(25));
    }

    @Test
    public void getMinInPairWithEqualItems(){
        Pair<Integer> pair = new Pair<>(5, 5);
        Assert.assertEquals(pair.min(), Integer.valueOf(5));
    }

    @AllArgsConstructor
    @Getter
    @Setter
    class Person implements Comparable {
        private String name;
        private int age;

        @Override
        public int compareTo(Object other) {
            return Integer.compare(this.age, ( (Person) other).age);
        }
    }


}
