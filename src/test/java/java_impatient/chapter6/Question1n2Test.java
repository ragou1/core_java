package java_impatient.chapter6;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.stream.IntStream;

public class Question1n2Test {
    public Stack<String> stack;

    @BeforeMethod
    public void beforeMethod(){
        stack = new Stack<>(5);
    }

    @Test
    public void poppingEmptyStack(){
        Assert.assertTrue(stack.pop().isEmpty());
    }

    @Test
    public void emptyStackHasNoItems(){
        Assert.assertTrue(stack.isEmpty());
    }

    @Test
    public void pushAndPopOneItem(){
        stack.push("hello");
        Assert.assertEquals(stack.pop().get(), "hello");
    }

    @Test(description = "push many items, but pop only one.")
    public void pushManyAndPopOne(){
        stack.push("hello");
        stack.push("cool");
        Assert.assertEquals(stack.pop().get(), "cool");
        Assert.assertEquals(stack.pop().get(), "hello");
    }

    @Test
    public void pushIntoFullStack(){
        IntStream.rangeClosed(1, stack.getSize()).mapToObj(i -> i + "").forEach(stack::push);
        stack.push("6");
        Assert.assertEquals(stack.pop().get(), "6" );
    }

}
