package java_impatient.chapter6;

import lombok.Getter;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*
* Implement a method map that receives an array list and a Function<T, R> object (see Chapter 3), and
* that returns an array list consisting of the results of applying the function to the given elements.
* */
public class Question15Test {
    @Test
    public void mapsIntegersToTheirSquares(){
        Supplier<IntStream> testData = () -> IntStream.rangeClosed(1, 5);
        ArrayList<Integer> nums = testData.get().boxed().collect(Collectors.toCollection(ArrayList::new));
        Function<Integer, Integer> square = z -> z * z;

        ArrayList<Integer> squares = Mapper.map(nums, square);

        testData.get().forEach( i -> Assert.assertEquals(square.apply(nums.get(i-1)), squares.get(i-1)) );
    }

    @Test
    public void mapsCustomObjects(){
        @Getter
        class Person {
            private Integer money;
            public Person(Integer money){this.money = money;}
            public void tax(Integer amount){ money -= amount;}
        }

        int [] moneys = {100, 200, 50, 1000};
        ArrayList<Person> persons = Arrays.stream(moneys)
                .mapToObj(Person::new)
                .collect(Collectors.toCollection(ArrayList::new));
        int taxAmount = 10;
        Function<Person, Person> tax = p -> {p.tax(taxAmount); return p;};

        ArrayList<Person> taxedPersons = Mapper.map(persons, tax);

        IntStream.range(0, moneys.length)
                .forEach( i ->
                        Assert.assertEquals(taxedPersons.get(i).getMoney(), Integer.valueOf(moneys[i]-taxAmount)) );

    }

}
