package java_impatient.chapter7;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/*
* How do you compute the union, intersection, and difference of two sets, using just the methods
* of the Set interface and without using loops?
* */
public class Question3Test {
    private final Set<Integer> set1 = new HashSet<>(Arrays.asList(1,2,3,4,5));
    private final Set<Integer> set2 = new HashSet<>(Arrays.asList(4,5,6,7));

    @Test(description = "Set.addAll(...) gives UNION.")
    public void testUnionOfSets(){
        Set<Integer> expected = new HashSet<>(Arrays.asList(1,2,3,4,5,6,7));
        Set<Integer> actual = new HashSet<>(set1);
        actual.addAll(set2);
        Assert.assertEquals(actual, expected);
    }

    @Test(description = "Set.retainAll(...) gives INTERSECTION.")
    public void testIntersectionOfSets(){
        Set<Integer> expected = new HashSet<>(Arrays.asList(4,5));
        Set<Integer> actual = new HashSet<>(set1);
        actual.retainAll(set2);
        Assert.assertEquals(actual, expected);
    }

    @Test(description = "Set.removeAll(...) gives DIFFERENCE.")
    public void testDifferenceOfSets(){
        Set<Integer> expected = new HashSet<>(Arrays.asList(1,2,3));
        Set<Integer> actual = new HashSet<>(set1);
        actual.removeAll(set2);
        Assert.assertEquals(actual, expected);
    }
}
