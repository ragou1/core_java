package java_impatient.chapter7;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
* Write a program that reads a sentence into an array list. Then, using Collections.shuffle,
* shuffle all but the first and last word, without copying the words into another collection.
* */
public class Question11Test {
    private void specialShuffle(List<String> list) {
        List<String> view =  list.subList(1, list.size()-1);
        Collections.shuffle(view);
    }

    @Test
    public void shuffling(){
        String sentence = "Come with me if you want to live";
        String [] words = sentence.split(" ");
        List<String> list = new ArrayList<>(Arrays.asList(words));
        specialShuffle(list);

        Assert.assertEquals(list.get(0), words[0]);
        Assert.assertEquals(list.get(list.size()-1), words[words.length-1]);

        //Some words might remain in original place after shuffling.
        int shuffledWordsCount = 0;

        for(int i = 1; i < words.length-2; i++){
            if(! list.get(i).equals(words[i]) ){
                shuffledWordsCount++;
            }
        }

        //Is there a better way to check for shuffling ?
        Assert.assertTrue(shuffledWordsCount > list.size()/2);
    }

}
