package java_impatient.chapter7;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/*
* In an array list of strings, make each string uppercase. Do this with (a) an iterator, (b) a loop over
* the index values, and (c) the replaceAll method.
* */
public class Question2Test {
    public static void main(String [] args) {
        List<String> strings = null;

        //(a) 1 : Using an Iterator.
        strings = Arrays.asList("1", "two", "three");
        Iterator<String> iterator = strings.iterator();
        while(iterator.hasNext()){
            String next = iterator.next();
            strings.set(strings.indexOf(next), next.toUpperCase());
        }
        System.out.println("(a) 1 : " + strings);

        //(a) 2 : Using an ListIterator.
        strings = Arrays.asList("1", "two", "three");
        ListIterator<String> litr = strings.listIterator();
        while (litr.hasNext()){
            litr.set(litr.next().toUpperCase());
        }
        System.out.println("(a) 2 : " + strings);

        //(b)
        strings = Arrays.asList("one", "2", "three");
        for(int i = 0; i < strings.size(); i++){
            strings.set(i, strings.get(i).toUpperCase());
        }
        System.out.println("(b) : " + strings);

        //(c)
        strings = Arrays.asList("one", "two", "3");
        strings.replaceAll(String::toUpperCase);
        System.out.println("(c) : " + strings);
    }
}
