package java_impatient.chapter7;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class Question4Test {
    @Test(expectedExceptions = ConcurrentModificationException.class)
    public void removeElementsDirectlyFromListDuringIteration() {
        List<Integer> integers = new ArrayList<>();
        // integers = Arrays.asList(1, 2, 3);// Won't work ! Why ?
        integers.add(1);
        integers.add(2);
        integers.add(3);

        //Btw, try putting the code in ending comments here and see what happens.

        for (Integer integer : integers) {
            integers.remove(1);//Throws ConcurrentModificationException.
        }
    }

    /*
     * The fix is to use an iterator, and use its methods to iterate *and* remove elements at
     * the same time. Like this :
     *
     * Iterator<Integer> iterator = integers.iterator();
     * while (iterator.hasNext()) {
     *    if (iterator.next() == 2) {
     *        iterator.remove();
     *    }
     * }
     *
     * */
}
