package java_impatient.chapter1;

public class Question2 {

    /*
    * Write a program that reads an integer angle (which may be positive or negative) and normalizes
    * it to a value between 0 and 359 degrees. Try it first with the % operator, then with floorMod.
    * */
    public static int normalizeAnglesV1(int angleInDegrees){
        int remainder = angleInDegrees % 360;
        return (remainder + 360) % 360; //Make any -ve remainder +ve.
    }

    public static int normalizeAnglesV2(int angleInDegrees){
        return Math.floorMod(angleInDegrees, 360);
    }

}

