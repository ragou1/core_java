package java_impatient.chapter4;

import java.io.IOException;

public class Question2 {
    public static double sumOfValues(String filename) throws IOException {
        return Question1.readValues(filename).stream().reduce(Double::sum).get();
    }
}
