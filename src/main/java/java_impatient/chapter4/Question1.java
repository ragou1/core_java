package java_impatient.chapter4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Question1 {
    /* Write a method public ArrayList<Double> readValues(String filename) throws ... that reads a file
     * containing floating-point numbers. Throw appropriate exceptions if the file could not be opened
     * or if some of the inputs are not floating-point numbers.
     */
    public static List<Double> readValues(String fullFilePath) throws IOException {
        List<Double> values = new ArrayList<>();

        try (BufferedReader fileReader = new BufferedReader( new FileReader(fullFilePath) ) ) {
            String line;

            while ( ( line = fileReader.readLine() ) != null ){
                Arrays
                        .stream(line.split(","))
                        .map(String::trim)
                        .filter(s -> Double.valueOf(s) instanceof Double)
                        .map(Double::valueOf)
                        .forEach(values::add);
            }
        }
        return values;
    }

}
