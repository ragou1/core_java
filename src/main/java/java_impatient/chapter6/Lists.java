package java_impatient.chapter6;

import java.util.Comparator;
import java.util.List;

public class Lists {/*
 * Implement the below method that stores the smallest and largest element in elements in the result list.
 * Method signature already given in book.
 * */

    public static <T> void minmax(List<T> elements, Comparator<? super T> comp, List<? super T> result) {
        elements.sort(comp);
        result.add(elements.get(0));
        result.add(elements.get(elements.size() - 1));
    }
}