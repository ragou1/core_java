package java_impatient.chapter6;

public class Pair<E extends Comparable> {
    private E x;
    private E y;

    public Pair(E x, E y) {
        this.x = x;
        this.y = y;
    }

    public E getFirst() {
        return this.x;
    }

    public E getSecond() {
        return this.y;
    }

    public E max() {
       int r = x.compareTo(y);
       if(r == 0){return x;}//x == y.
       return (r > 0) ? x : y;//max
    }

    public E min() {
        int r = x.compareTo(y);
        if(r == 0){return x;}//x == y.
        return (r < 0) ? x : y;//min
    }
}
