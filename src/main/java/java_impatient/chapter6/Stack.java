package java_impatient.chapter6;

import java.util.Arrays;
import java.util.Optional;

public class Stack<T> {
    private T[] storage;
    private static final int CAPACITY_INCREMENT = 10;
    private int currentIdx = -1;

    public Stack() {
        storage = (T[]) new Object[CAPACITY_INCREMENT];
    }

    public Stack(int numberOfElements) {
        storage = (T[]) new Object[numberOfElements];
    }

    public void push(T element) {
        if(isFull()){
            increaseStorageSize(CAPACITY_INCREMENT);
        }
        currentIdx++;
        storage[currentIdx] = element;
    }

    private void increaseStorageSize(int increaseBy){
        storage = Arrays.copyOf(storage, storage.length + increaseBy);
    }

    public Optional<T> pop() {
        if(isEmpty()){
            return Optional.empty();
        }
        T item = storage[currentIdx];
        currentIdx--;
        return Optional.of(item);
    }

    public boolean isEmpty() {
        return (currentIdx < 0) ? true : false;
    }

    private boolean isFull() { return currentIdx == storage.length-1 ? true : false; }

    public int getSize() {
        return isEmpty() ? 0 : currentIdx;
    }
}
