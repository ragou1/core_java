package java_impatient.chapter6;

import java.util.ArrayList;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Mapper {
    public static <T, R> ArrayList<R> map(ArrayList<T> items, Function<T, R> mapper) {
        return items.stream().map(mapper).collect(Collectors.toCollection(ArrayList::new));
    }
}
