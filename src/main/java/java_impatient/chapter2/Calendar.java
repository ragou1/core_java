package java_impatient.chapter2;

import java.time.DayOfWeek;
import java.time.LocalDate;

//TODO : Add tests.
public class Calendar {
    public static void main(String[] args) {
        printCalendar(3, 2020);
    }

    public static void printCalendar(int month, int year) {
        LocalDate date = LocalDate.of(year, month, 1);

        System.out.println(" Mon Tue Wed Thu Fri Sat Sun");
        DayOfWeek weekday = date.getDayOfWeek();
        int value = weekday.getValue(); // 1 = Monday, ... 7 = Sunday

        for (int i = 1; i < value; i++) {
            System.out.print("    ");
        }

        while (date.getMonthValue() == month) {
            System.out.printf("%4d", date.getDayOfMonth());
            date = date.plusDays(1);
            if (date.getDayOfWeek().getValue() == 1) {
                System.out.println();
            }
        }

        if (date.getDayOfWeek().getValue() != 1) {
            System.out.println();
        }
    }

}
